Documentation : 
Liens vers les sites ayant servi à construire cette documentation :

https://doc.ubuntu-fr.org/dokuwiki

https://www.google.com/

https://crontab-generator.org/

https://www.grafikart.fr/formations/serveur-linux/apache-ssl-letsencrypt

site web crée : https://www.dokuwiki.geeked.fr/install.php

A) Connexion au serveur et mise en place du service

# Sur le terminal, taper la commande suivante : 
~~~~
ssh-keygen
~~~~

# Envoi de la clé sur l’adresse mail yanis.boucherit@gmail.com .

Création par Yanis de machines virtuelles hébergées sur AWS, utilisant le système d’exploitation Ubuntu 18.04.

# Se connecter aux machines virtuelles.

Sur le terminal, taper la commande suivante : 
~~~~
ssh -i [Chemin_vers_clé_privée] simplon@ec2-52-47-208-23.eu-west-3.compute.amazonaws.com

ssh -i ~/lundi_9_juillet/lundi9juillet simplon@52.47.155.207
~~~~

Connexion à simplon@ip-172-31-46-65.

# Héberger le service Dokuwiki sur la machine virtuelle

## Installation du service

*Pour obtenir l’archive du service*
~~~~
wget https://download.dokuwiki.org/src/dokuwiki/dokuwiki-2018-04-22.tgz
~~~~

*Installation d’apache*
~~~~
sudo apt install apache2
~~~~

*Installation de PHP 7*
~~~~
sudo apt install php ( => php 7 )
~~~~

*Création du repository et depackaging du service*
~~~~
tar -xvzf dokuwiki-2018-04-22.tgz 
~~~~

*Renomer le dossier dokuwiki-2018-04-22 en dokuwiki*
~~~~
mv dokuwiki-2018-04-22 dokuwiki
~~~~

*Configuration d'apache2 pour le VirtualHost*
~~~~
sudo mkdir /var/www/dokuwiki.geeked.fr
sudo mkdir /var/www/dokuwiki.geeked.fr/www
sudo mkdir /var/www/dokuwiki.geeked.fr/logs
sudo touch /etc/apache2/sites-available/dokuwiki.geeked.fr.conf
sudo vim /etc/apache2/sites-available/dokuwiki.geeked.fr.conf
sudo a2ensite dokuwiki.geeked.fr
sudo service apache2 reload
~~~~

*Déplacement dans le workspace d’apache*
~~~~
sudo cp -R dokuwiki/* /var/www/dokuwiki.geeked.fr/www/
~~~~

*Modifier propriétaire et groupe du dossier*
~~~~
sudo chown -R www-data:www-data /var/www/html/dokuwiki/
~~~~

## firewall

*Tout le monde à accès à l’install.php!! => mettre un firewall!!*
*Pour connaitre son ip => mon-ip.com*
~~~~
sudo ufw allow from [adresseip publique etincelle] proto tcp to any port 80
sudo ufw allow ssh
sudo ufw enable
sudo service ufw status
sudo service ufw start
~~~~

## HTTPS

*Mise en place du HTTPS*
Utilisation de let's encrypt 

### installer git
~~~~
sudo apt-get update
sudo apt-get install git
~~~~

### import du projet depuis github
~~~~
sudo git clone https://github.com/letsencrypt/letsencrypt /opt/letsencrypt --depth=1
~~~~

### mise en place du certificat

Cette commande va installer les dépendances du client, elle va ensuite détecter les noms de domaine configurés sur votre serveur apache, demander les certificats pour ces derniers et enfin modifier la configuration de vos différents VirtualHosts : 

~~~~
/opt/letsencrypt/letsencrypt-auto --authenticator standalone --installer apache -d mondomaine.fr --pre-hook "service apache2 stop" --post-hook "service apache2 start"
~~~~

Les certificats générés ainsi que les clés privées sont stockés dans le dossier /etc/letsencrypt/live/

### renouvellement tous les 90 jours

~~~~
/opt/letsencrypt/letsencrypt-auto --authenticator standalone --installer apache -d mondomaine.fr --pre-hook "service apache2 stop" --post-hook "service apache2 start" --renew-by-default
~~~~

Des visiteurs !! => suspect...
~~~~
sudo apt-get install whois
whois
~~~~

Holy shit : un américain et un russe ...

*date de creation et de changement de fichier/dossier*
~~~~
stat [nom_du_fichier]
~~~~

*Pour le ssl*
~~~~
<IfModule mod_ssl.c>
<VirtualHost *:443>
        ServerAdmin haingo.rajaon@gmail.com
        ServerName dokuwiki.geeked.fr

        DocumentRoot /var/www/dokuwiki.geeked.fr/www
        <Directory /var/www/dokuwiki.geeked.fr/www>
                Options +FollowSymLinks
                AllowOverride AuthConfig Options
		require all granted
        </Directory>

        ErrorLog /var/www/dokuwiki.geeked.fr/logs/error.log

        LogLevel warn

        CustomLog /var/www/dokuwiki.geeked.fr/logs/access.log combined


SSLCertificateFile /etc/letsencrypt/live/dokuwiki.geeked.fr/fullchain.pem
SSLCertificateKeyFile /etc/letsencrypt/live/dokuwiki.geeked.fr/privkey.pem
Include /etc/letsencrypt/options-ssl-apache.conf

</VirtualHost>
</IfModule>
~~~~

*Configuration du fichier russian_are_dangerous.conf*
~~~~
<VirtualHost *:80>
	ServerName [adresse ip publique serveur hebergeur]
	
	<Directory /var/www/html/>
		Require all denied
	</Directory>
</VirtualHost>
~~~~

*avant le let's encrypt => port HTTP (80)*
dokuwiki.geeked.fr.conf 
~~~~
<VirtualHost *:80>
        ServerAdmin haingo.rajaon@gmail.com
        ServerName dokuwiki.geeked.fr

        DocumentRoot /var/www/dokuwiki.geeked.fr/www
        <Directory /var/www/dokuwiki.geeked.fr/www>
                Options +FollowSymLinks
                AllowOverride AuthConfig Options
		require all granted
        </Directory>

        ErrorLog /var/www/dokuwiki.geeked.fr/logs/error.log

        LogLevel warn

        CustomLog /var/www/dokuwiki.geeked.fr/logs/access.log combined
RewriteEngine on
RewriteCond %{SERVER_NAME} =dokuwiki.geeked.fr
RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
~~~~

=> PLUS DE SITE!!

### Redirection du http vers https

Il existe deux types de dossiers : celui qui déclare les sites hébergés par le serveur (sites-available) et les sites disponibles sur la toile (sites-enabled)

## Outils de monitoring

Choix de munin car il nous reste seulement deux heures, et l'installation de centreon semble fastidieuse.

http://guide.munin-monitoring.org/en/latest/installation/install.html

L'architecture de munin se compose d'une machine maîtresse et d'une machine observée.

Machine de monitoring => munin master

Machine supervisée => munin node

Machine superviseur : Haingo

**etape a) installation de munin sur machine supervisée**

Pour pouvoir superviser le superviseur :
~~~~
sudo apt install munin munin-node munin-plugins-extra libcgi-fast-perl 
~~~~

~~~~
sudo a2enmod fcgid
~~~~

~~~~
sudo service apache2 reload
~~~~

Modification du fichier /etc/munin/apache24.conf :
~~~~
Alias /munin /var/cache/munin/www
<Directory /var/cache/munin/www>

	<RequireAny>

        	Require local
        	Require ip [adresseip publique etincelle]

	</RequireAny>

        Options None

</Directory>
~~~~

~~~~
ScriptAlias /munin-cgi/munin-cgi-graph /usr/lib/munin/cgi/munin-cgi-graph
<Location /munin-cgi/munin-cgi-graph>
	<RequireAny>

        	Require local
        	Require ip [adresseip publique etincelle]

	</RequireAny>
        <IfModule mod_fcgid.c>

            SetHandler fcgid-script

        </IfModule>

        <IfModule !mod_fcgid.c>

            SetHandler cgi-script

        </IfModule>

</Location>
~~~~

installation de munin sur machine supervisée

**etape b) installation de munin sur machine supervisée**

~~~~
sudo apt install munin-node munin-plugins-extra
~~~~

**etape c) choix des données à "monitorer" graphiquement**

Pour que le grapheur accède aux informations du noeud :
~~~~
/etc/munin/munin-node.conf
allow ^[adresseIP privée de la machine superviseur]$
~~~~

**etape d) les plugins**

Les plugins Apache doivent être activés via la commande :
~~~~
sudo a2enmod status
~~~~

visualiser les plugins installés : 
~~~~
munin-node-configure
~~~~

visualiser les suggestions :
~~~~
munin-node-configure --suggest
~~~~

/etc/munin/munin.conf

~~~~
[dokuwiki.geeked.fr]
 address 172.31.33.12
   use_node_name yes
~~~~

**etape e) test des plugins**

~~~~
sudo netstat -lataupen
~~~~

port et protocole autorisés entre master et nodes

Interface : [adresseIP publique machine superviseur/munin/]

Siege linux : stress test, à tester

**etape f) Backup :**

Script exécuté à midi tous les jours :
~~~~
touch backup.sh
sudo chmod 700 backup.sh
~~~~

~~~~
crontab -e
~~~~

*Dans crontab, ajouter*
~~~~
0 12 * * * ./etc/cron.daily/backup.sh
~~~~

après le partage de la clé publique dans .ssh/authorized_keys
~~~~
scp -p /home/simplon/test/txt simplon@[IPPrivé]:~/Backup
~~~~

~~~~
#!/bin/bash

#date du jour
backupdate=$(date +%d-%m-%Y)

#repertoire de backup
filename=dokuwiki_backup_$backupdate.tar.gz
cd /var/www

#creation de l'archive backup
tar zcfv ~/backup/$filename dokuwiki.geeked.fr/

#envoi de l'archive sur le serveur backup
scp -p -i ~/.ssh/id_rsa ~/backup/$filename simplon@172.31.43.56:/home/simplon/Backup/

#netoyage du dossier backup
rm ~/backup/*
~~~~

